//
//  CustomBreakOutSessionCell.h
//  AwayDay2012
//
//  Created by safadmoh on 14/09/13.
//
//

#import <UIKit/UIKit.h>

@interface CustomBreakOutSessionCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *topicTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicSpeakerNameTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeTextLabel;

@end
