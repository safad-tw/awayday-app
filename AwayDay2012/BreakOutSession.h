//
//  BreakOutSession.h
//  AwayDay2012
//
//  Created by apurvagu on 16/09/13.
//
//

#import <Foundation/Foundation.h>

@interface BreakOutSession : NSObject

@property(nonatomic, strong) NSString *trackTopic;
@property(nonatomic, strong) NSString *captainName;
@property(nonatomic, strong) NSArray *topics;

@end
