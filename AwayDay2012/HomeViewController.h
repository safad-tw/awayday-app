//
//  HomeViewController.h
//  AwayDay2012
//
//  Created by safadmoh on 11/09/13.
//
//

#import <UIKit/UIKit.h>



@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *counterTextLabel;

- (IBAction)onBurger:(id)sender;

@end
